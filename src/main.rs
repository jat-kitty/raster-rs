extern crate num_complex;

use num_complex::Complex;

use std::thread;
use std::sync::{Arc, Mutex};

mod farbfeld;

use farbfeld::Image;


const MAX_ITER: u16 = 256;

const WIDTH: usize = 2560;
const HEIGHT: usize = 2048;


fn main() {
	println!("» creating image");
	let mut _image = Image::new(WIDTH, HEIGHT);
	let image = Arc::new(Mutex::new(_image));

	// mandelbrot, because - well - why not?
	let mut handles = vec![];

	for c in 0..16 {
		let x1 = WIDTH / 16 * c; let x2 = WIDTH / 16 * (c + 1);
		
		println!("» Spawning thread nr. {}", c);
		println!("-» for ({}|{}) to ({}|{})", x1, 0, x2, HEIGHT);

		let img = Arc::clone(&image);
		let handle = thread::spawn(move || {
			for i in x1..x2 {
				for j in 0..HEIGHT {
					let cx = -2_f32 + i as f32 * ((1_f32 - -2_f32) / WIDTH as f32);
					let cy = -1.2_f32 + j as f32 * ((1.2_f32 - -1.2_f32) / HEIGHT as f32);

					let c = Complex::new(cx, cy);
					let mut z = Complex::new(0_f32, 0_f32);

					let mut iter = 0;
					for t in 0..MAX_ITER {
						if z.norm() > 2.0 { break; }
						z = z.powi(2) + c;
						iter = t;
					}

					let r: u16; let g: u16; let b: u16;
					if z.norm_sqr() <= 4.0 { 
						r = 0; g = 0; b = 0;
					} else {
						if (iter as f32) < (MAX_ITER as f32 / 2.0 - 1.0) {
							let _factor = (iter as f32 / MAX_ITER as f32).sqrt();
							r = (std::u16::MAX as f32 * _factor) as u16;
							g = 0; b = 0;
						} else {
							let _factor = (iter as f32 / MAX_ITER as f32).sqrt();
							r = std::u16::MAX;
							g = (std::u16::MAX as f32 * _factor) as u16;
							b = (std::u16::MAX as f32 * _factor) as u16;
						}
					}

					let mut image = img.lock().unwrap();
					image.set_pixel(i, j, r, g, b, 0xffff);
				}
			}
		});

		handles.push(handle);
	}

	for handle in handles {
		handle.join().unwrap();
	}

	image.lock().unwrap().write_ff("foo.ff");
}
