use std::fs::File;
use std::io::Write;


pub struct Image {
	width: usize,
	height: usize,
	data: Vec<u16>,
}

impl Image {
	pub fn new(w: usize, h: usize) -> Image {
		Image {
			width: w,
			height: h,
			data: vec![0x0; w * h * 4],
		}
	}

	pub fn set_pixel(&mut self, _pos_x: usize, _pos_y: usize, r: u16, g: u16, b: u16, a: u16) {
		let i = (_pos_y * self.width + _pos_x) * 4;
		let mut pixel = [r, g, b, a];
		self.data[i..i + 4].swap_with_slice(&mut pixel);
	}

	fn to_ff(&self) -> Vec<u8> {
		let mut ff: Vec<u8> = Vec::with_capacity(16 + self.width * self.height * 8);
		
		// write header
		ff.extend_from_slice(b"farbfeld");
		ff.extend_from_slice(&(self.width as u32).to_be_bytes());
		ff.extend_from_slice(&(self.height as u32).to_be_bytes());

		// write data
		for dat in self.data.iter() {
			ff.extend_from_slice(&dat.to_be_bytes());
		}

		ff
	}

	pub fn write_ff(&self, path: &str) {
		let mut file = File::create(path).unwrap();

		file.write_all(&self.to_ff());
	}
}
